#
#/* A minimal program that writes all pages it gets to disk.
#
gcc cap-all.c -I.. -DVERSION=\"0.0.1\" \
    ../vbi.o ../misc.o ../fdset.o ../hamm.o ../lang.o \
    ../export.c ../exp-txt.o ../exp-html.o ../exp-gfx.c ../font.o
exit 0
#*/

#include "vt.h"
#include "fdset.h"
#include "vbi.h"
#include "export.h"

int debug = 0;

static void
event(void *fmt, struct vt_event *ev)
{
    if (ev->type == EV_PAGE)
    {
	struct vt_page *vtp = ev->p1;
	char *name = export_mkname(fmt, "ttext-%p.%e", vtp, "");
	
	export(fmt, vtp, name);
	free(name);
    }
}

int
main(int argc, char **argv)
{
    struct vbi *vbi;
    struct export *fmt1;
    struct export *fmt2;
    
    fdset_init(fds);

    vbi = vbi_open("/dev/vbi", 0, 1, -1);
    fmt1 = export_open("ascii");
    fmt2 = export_open("html");

    if (!fmt1 || !fmt2 || !vbi) exit(1);

    vbi_add_handler(vbi, event, fmt1);
    vbi_add_handler(vbi, event, fmt2);

    for (;;)
	fdset_select(fds, -1);
}
