.na
.TH ALEVT 1x "VERSION" LINUX "Teletext decoder"
.SH NAME
alevt \- X11 Teletext browser

.SH SYNOPSIS
.B alevt
.RI [ options ]

.SH "DESCRIPTION"
.B alevt
is an X11 program for browsing and searching Teletext pages received
by a compatible decoder (at the moment, bttv).

.SH OPTIONS
A summary of command line options is included below.  The X11 user
interface is described in the built-in manual.
.TP 4
.BI \-display \ display
Where to display the windows.  Default: $DISPLAY.
.TP
.BI \-geometry \ geometry
Specifies the size and position of the windows.  Default: 41x25.
.TP
.BI \-vbi \ device
Use the given device name.  Default: /dev/vbi, /dev/vbi0, /dev/video0, /dev/dvb/adapter0/demux0 tried in this order.
.TP
.BI \-finetune \ \-4..4\||auto
Try some fine tuning if you have a lot of decoding errors (these
lightning symbols).  This allows fine tuning of the decoder circuit.
The pre finetune behaviour was \-finetune 0.  Default: 1.
.TP
.BI [\-parent] \ ppp[.ss]
Open a new window with the page number given by
.I ppp
and an optional subpage index given by
.IR ss .

The
.B \-parent
keyword is just for completeness and not normally used.  The page number
alone is sufficient.  Default: 900.
.TP
.BI \-child \ ppp[.ss]
Open a new child window with the page number given by
.I ppp
and an optional subpage index given by
.IR ss .
About the concept of parent and child windows see the built-in manual.
.TP
.\" .B \-oldbttv
.\" Give this option if you are using the bttv driver from the 2.2 kernel
.\" or a very old one (<0.5.20) from the
.\" .B xawtv
.\" package.
.\" .TP
.BR \-erc / \-noerc
Enables/disables the error reduction code which substitutes
errors on a page by data from a previously received one.
Enabled by default.
.TP
.BR \-bell / \-nobell
Enables/disables the error bell.  Normally enabled.
.TP
.BI \-charset \ latin-1\||latin-2\||koi8-r\||iso8859-7
Selects the character set to use.  Characters not present
in the selected set are replaced by similar looking symbols.
(default: latin-1)
.TP
.BI \-progname \ name
Specifies the program name if using the DVB interface.
.TP
.BI \-pid \ pid
Specifies the teletext PID if using the DVB interface.
.TP
.B \-\-help
Show summary of options.
.TP
.B \-\-version
Show version of program.
.PP
The order of options is important!  Each page number opens a new window
with the previously given geometry, device, and display.  For example,
to open two windows on the local display and one on display foo:0 you would
start it with:
.IP
.I alevt 100 300 \-display foo:0 100
.PP
Or, one window from /dev/vbi0 and one from /dev/vbi1 (untested *g*):
.IP
.I alevt \-vbi /dev/vbi0 100  \-vbi /dev/vbi1 100
.PP
The
.B \-child
options requires a parent window.  So, it must be preceded by a parent
or another child window.
.PP
When saving pages
.B alevt
asks for a format string.  Look at
.BR alevt-cap (1).
The chapter
.B Format Specifiers
gives an explanation.
.PP
.B alevt
has no support to set or change the TV channel.  This
should be done with another (not supplied) program like
.B xawtv
or
.B set-tv
from the
.B xawtv
package.  Maybe in the future there will be a more
sophisticated method in bttv to switch channels etc with a dedicated
tuner application.  But, at the moment, you have to live with it...

The
.B \-progname
or
.B \-pid
option specifies the program or teletext PID if using the DVB interface.

If neither of these two options is used, alevt outputs informations about the currently accessible streams
and uses the first of these PIDs as default. This output contains the PIDs and names that can be used
as an input for the
.B \-progname
or
.B \-pid
option in the context of the currently chosen TV channel.

.SH FILES
.I /dev/vbi*

.I /dev/dvb/adapter*/demux*

.SH EXAMPLES
Select the teletext stream by the PID hex value:
.IP
.I alevt \-vbi /dev/dvb/adapter0/demux0 \-pid 0x0237
.PP
Select the teletext stream by the PID decimal value:
.IP
.I alevt \-vbi /dev/dvb/adapter0/demux0 \-pid 567
.PP
Select the teletext stream by the program name:
.IP
.I alevt \-vbi /dev/dvb/adapter0/demux0 \-progname \'kabel eins\'
.PP

.SH "SEE ALSO"
.BR alevt-date (1),
.BR alevt-cap (1),
.BR X (3x),
.BR xawtv (1x),
and the built-in manual.

.SH BUGS
Bug reports to <https://gitlab.com/alevt/alevt>.
